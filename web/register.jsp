<%-- 
    Document   : register
    Created on : May 14, 2022, 9:07:34 AM
    Author     : admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register form</title>
    </head>
    <body>
        <form>
            Username: <input type="text" name="username" placeholder="EX: abc"><br/>
            Password: <input type="text" name="password" placeholder="EX: abc"><br/>
            Email Address: <input type="text" name="email" placeholder="EX: abc@gmail.com"><br/>
            <input type="submit" value="Register">
        </form>
        <button><a href="">Already have account</a></button>
    </body>
</html>
